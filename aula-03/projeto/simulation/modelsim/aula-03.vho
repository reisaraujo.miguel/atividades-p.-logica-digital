-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 20.1.1 Build 720 11/11/2020 SJ Lite Edition"

-- DATE "05/29/2021 22:06:12"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	\aula-03\ IS
    PORT (
	V1 : OUT std_logic;
	S1 : IN std_logic;
	V2 : OUT std_logic;
	S2 : IN std_logic;
	V3 : OUT std_logic;
	S0 : IN std_logic;
	A : OUT std_logic
	);
END \aula-03\;

-- Design Ports Information
-- V1	=>  Location: PIN_Y3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- V2	=>  Location: PIN_G1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- V3	=>  Location: PIN_C2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A	=>  Location: PIN_G2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1	=>  Location: PIN_AA2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S0	=>  Location: PIN_N2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF \aula-03\ IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_V1 : std_logic;
SIGNAL ww_S1 : std_logic;
SIGNAL ww_V2 : std_logic;
SIGNAL ww_S2 : std_logic;
SIGNAL ww_V3 : std_logic;
SIGNAL ww_S0 : std_logic;
SIGNAL ww_A : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \S1~input_o\ : std_logic;
SIGNAL \S2~input_o\ : std_logic;
SIGNAL \inst~combout\ : std_logic;
SIGNAL \S0~input_o\ : std_logic;
SIGNAL \inst3~combout\ : std_logic;
SIGNAL \inst11~0_combout\ : std_logic;
SIGNAL \ALT_INV_S0~input_o\ : std_logic;
SIGNAL \ALT_INV_S2~input_o\ : std_logic;
SIGNAL \ALT_INV_S1~input_o\ : std_logic;
SIGNAL \ALT_INV_inst3~combout\ : std_logic;

BEGIN

V1 <= ww_V1;
ww_S1 <= S1;
V2 <= ww_V2;
ww_S2 <= S2;
V3 <= ww_V3;
ww_S0 <= S0;
A <= ww_A;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_S0~input_o\ <= NOT \S0~input_o\;
\ALT_INV_S2~input_o\ <= NOT \S2~input_o\;
\ALT_INV_S1~input_o\ <= NOT \S1~input_o\;
\ALT_INV_inst3~combout\ <= NOT \inst3~combout\;

-- Location: IOOBUF_X0_Y18_N45
\V1~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \S1~input_o\,
	devoe => ww_devoe,
	o => ww_V1);

-- Location: IOOBUF_X0_Y21_N22
\V2~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst~combout\,
	devoe => ww_devoe,
	o => ww_V2);

-- Location: IOOBUF_X0_Y21_N39
\V3~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_inst3~combout\,
	devoe => ww_devoe,
	o => ww_V3);

-- Location: IOOBUF_X0_Y21_N5
\A~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst11~0_combout\,
	devoe => ww_devoe,
	o => ww_A);

-- Location: IOIBUF_X0_Y18_N78
\S1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S1,
	o => \S1~input_o\);

-- Location: IOIBUF_X0_Y21_N55
\S2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S2,
	o => \S2~input_o\);

-- Location: LABCELL_X1_Y21_N0
inst : cyclonev_lcell_comb
-- Equation(s):
-- \inst~combout\ = ( \S1~input_o\ & ( \S2~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000011110000111100000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_S2~input_o\,
	datae => \ALT_INV_S1~input_o\,
	combout => \inst~combout\);

-- Location: IOIBUF_X0_Y19_N38
\S0~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S0,
	o => \S0~input_o\);

-- Location: LABCELL_X1_Y21_N9
inst3 : cyclonev_lcell_comb
-- Equation(s):
-- \inst3~combout\ = ( \S1~input_o\ ) # ( !\S1~input_o\ & ( (\S2~input_o\) # (\S0~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010111111111111111111111111101010101111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_S0~input_o\,
	datad => \ALT_INV_S2~input_o\,
	datae => \ALT_INV_S1~input_o\,
	combout => \inst3~combout\);

-- Location: LABCELL_X1_Y21_N12
\inst11~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst11~0_combout\ = ( \S1~input_o\ & ( !\S0~input_o\ ) ) # ( !\S1~input_o\ & ( \S2~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101111100001111000001010101010101011111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_S2~input_o\,
	datac => \ALT_INV_S0~input_o\,
	datae => \ALT_INV_S1~input_o\,
	combout => \inst11~0_combout\);

-- Location: LABCELL_X7_Y15_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


