-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 20.1.1 Build 720 11/11/2020 SJ Lite Edition"

-- DATE "06/01/2021 09:17:20"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	\aula-04\ IS
    PORT (
	CD : OUT std_logic;
	F : IN std_logic;
	D : IN std_logic;
	CE : OUT std_logic;
	E : IN std_logic;
	CF : OUT std_logic
	);
END \aula-04\;

-- Design Ports Information
-- CD	=>  Location: PIN_AA2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CE	=>  Location: PIN_AA1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- CF	=>  Location: PIN_W2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- F	=>  Location: PIN_V13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D	=>  Location: PIN_T12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- E	=>  Location: PIN_T13,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF \aula-04\ IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CD : std_logic;
SIGNAL ww_F : std_logic;
SIGNAL ww_D : std_logic;
SIGNAL ww_CE : std_logic;
SIGNAL ww_E : std_logic;
SIGNAL ww_CF : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \F~input_o\ : std_logic;
SIGNAL \D~input_o\ : std_logic;
SIGNAL \inst2~combout\ : std_logic;
SIGNAL \E~input_o\ : std_logic;
SIGNAL \inst5~combout\ : std_logic;
SIGNAL \ALT_INV_E~input_o\ : std_logic;
SIGNAL \ALT_INV_D~input_o\ : std_logic;
SIGNAL \ALT_INV_F~input_o\ : std_logic;

BEGIN

CD <= ww_CD;
ww_F <= F;
ww_D <= D;
CE <= ww_CE;
ww_E <= E;
CF <= ww_CF;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_E~input_o\ <= NOT \E~input_o\;
\ALT_INV_D~input_o\ <= NOT \D~input_o\;
\ALT_INV_F~input_o\ <= NOT \F~input_o\;

-- Location: IOOBUF_X0_Y18_N79
\CD~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2~combout\,
	devoe => ww_devoe,
	o => ww_CD);

-- Location: IOOBUF_X0_Y18_N96
\CE~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst5~combout\,
	devoe => ww_devoe,
	o => ww_CE);

-- Location: IOOBUF_X0_Y18_N62
\CF~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_F~input_o\,
	devoe => ww_devoe,
	o => ww_CF);

-- Location: IOIBUF_X33_Y0_N58
\F~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_F,
	o => \F~input_o\);

-- Location: IOIBUF_X34_Y0_N18
\D~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D,
	o => \D~input_o\);

-- Location: MLABCELL_X28_Y4_N30
inst2 : cyclonev_lcell_comb
-- Equation(s):
-- \inst2~combout\ = ( \F~input_o\ & ( !\D~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \ALT_INV_F~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \inst2~combout\);

-- Location: IOIBUF_X34_Y0_N1
\E~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_E,
	o => \E~input_o\);

-- Location: MLABCELL_X28_Y4_N9
inst5 : cyclonev_lcell_comb
-- Equation(s):
-- \inst5~combout\ = ( \F~input_o\ & ( \D~input_o\ & ( !\E~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000001010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_E~input_o\,
	datae => \ALT_INV_F~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \inst5~combout\);

-- Location: LABCELL_X17_Y17_N3
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


