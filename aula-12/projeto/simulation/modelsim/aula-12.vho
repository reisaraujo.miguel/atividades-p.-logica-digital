-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 20.1.1 Build 720 11/11/2020 SJ Lite Edition"

-- DATE "07/26/2021 21:15:55"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	\aula-12\ IS
    PORT (
	R1 : OUT std_logic;
	B4 : IN std_logic;
	B3 : IN std_logic;
	B2 : IN std_logic;
	B1 : IN std_logic;
	A4 : IN std_logic;
	A3 : IN std_logic;
	A2 : IN std_logic;
	A1 : IN std_logic;
	R2 : OUT std_logic;
	R3 : OUT std_logic;
	R4 : OUT std_logic;
	R5 : OUT std_logic;
	Q1 : OUT std_logic;
	Q2 : OUT std_logic;
	Q3 : OUT std_logic;
	Q4 : OUT std_logic
	);
END \aula-12\;

-- Design Ports Information
-- R1	=>  Location: PIN_C2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R2	=>  Location: PIN_W2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R3	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R4	=>  Location: PIN_N2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R5	=>  Location: PIN_H6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q1	=>  Location: PIN_U7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q2	=>  Location: PIN_AA1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q3	=>  Location: PIN_G2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q4	=>  Location: PIN_U2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1	=>  Location: PIN_Y3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A4	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B4	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B1	=>  Location: PIN_N1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B3	=>  Location: PIN_D3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B2	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A3	=>  Location: PIN_AA2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2	=>  Location: PIN_G1,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF \aula-12\ IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_R1 : std_logic;
SIGNAL ww_B4 : std_logic;
SIGNAL ww_B3 : std_logic;
SIGNAL ww_B2 : std_logic;
SIGNAL ww_B1 : std_logic;
SIGNAL ww_A4 : std_logic;
SIGNAL ww_A3 : std_logic;
SIGNAL ww_A2 : std_logic;
SIGNAL ww_A1 : std_logic;
SIGNAL ww_R2 : std_logic;
SIGNAL ww_R3 : std_logic;
SIGNAL ww_R4 : std_logic;
SIGNAL ww_R5 : std_logic;
SIGNAL ww_Q1 : std_logic;
SIGNAL ww_Q2 : std_logic;
SIGNAL ww_Q3 : std_logic;
SIGNAL ww_Q4 : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \A4~input_o\ : std_logic;
SIGNAL \B4~input_o\ : std_logic;
SIGNAL \B1~input_o\ : std_logic;
SIGNAL \B3~input_o\ : std_logic;
SIGNAL \inst32|inst4~0_combout\ : std_logic;
SIGNAL \B2~input_o\ : std_logic;
SIGNAL \A3~input_o\ : std_logic;
SIGNAL \inst40|inst23|inst3~0_combout\ : std_logic;
SIGNAL \A2~input_o\ : std_logic;
SIGNAL \inst40|inst23|inst3~1_combout\ : std_logic;
SIGNAL \inst26|inst4~0_combout\ : std_logic;
SIGNAL \inst40|inst23|inst3~2_combout\ : std_logic;
SIGNAL \inst32|inst4~1_combout\ : std_logic;
SIGNAL \A1~input_o\ : std_logic;
SIGNAL \inst30|inst4~0_combout\ : std_logic;
SIGNAL \inst31|inst4~0_combout\ : std_logic;
SIGNAL \inst36|inst23|inst5~0_combout\ : std_logic;
SIGNAL \inst34|inst4~0_combout\ : std_logic;
SIGNAL \inst35|inst23|inst8~0_combout\ : std_logic;
SIGNAL \inst35|inst4~0_combout\ : std_logic;
SIGNAL \inst35|inst23|inst5~0_combout\ : std_logic;
SIGNAL \inst36|inst4~0_combout\ : std_logic;
SIGNAL \inst37|inst4~0_combout\ : std_logic;
SIGNAL \inst37|inst23|inst5~0_combout\ : std_logic;
SIGNAL \inst39|inst23|inst3~0_combout\ : std_logic;
SIGNAL \inst25|inst23|inst5~combout\ : std_logic;
SIGNAL \ALT_INV_A2~input_o\ : std_logic;
SIGNAL \ALT_INV_A3~input_o\ : std_logic;
SIGNAL \ALT_INV_B2~input_o\ : std_logic;
SIGNAL \ALT_INV_B3~input_o\ : std_logic;
SIGNAL \ALT_INV_B1~input_o\ : std_logic;
SIGNAL \ALT_INV_B4~input_o\ : std_logic;
SIGNAL \ALT_INV_A4~input_o\ : std_logic;
SIGNAL \ALT_INV_A1~input_o\ : std_logic;
SIGNAL \inst25|inst23|ALT_INV_inst5~combout\ : std_logic;
SIGNAL \inst39|inst23|ALT_INV_inst3~0_combout\ : std_logic;
SIGNAL \inst37|inst23|ALT_INV_inst5~0_combout\ : std_logic;
SIGNAL \inst35|inst23|ALT_INV_inst5~0_combout\ : std_logic;
SIGNAL \inst35|inst23|ALT_INV_inst8~0_combout\ : std_logic;
SIGNAL \inst32|ALT_INV_inst4~1_combout\ : std_logic;
SIGNAL \inst40|inst23|ALT_INV_inst3~2_combout\ : std_logic;
SIGNAL \inst32|ALT_INV_inst4~0_combout\ : std_logic;
SIGNAL \inst36|inst23|ALT_INV_inst5~0_combout\ : std_logic;
SIGNAL \inst30|ALT_INV_inst4~0_combout\ : std_logic;
SIGNAL \inst31|ALT_INV_inst4~0_combout\ : std_logic;
SIGNAL \inst40|inst23|ALT_INV_inst3~1_combout\ : std_logic;
SIGNAL \inst40|inst23|ALT_INV_inst3~0_combout\ : std_logic;
SIGNAL \inst26|ALT_INV_inst4~0_combout\ : std_logic;

BEGIN

R1 <= ww_R1;
ww_B4 <= B4;
ww_B3 <= B3;
ww_B2 <= B2;
ww_B1 <= B1;
ww_A4 <= A4;
ww_A3 <= A3;
ww_A2 <= A2;
ww_A1 <= A1;
R2 <= ww_R2;
R3 <= ww_R3;
R4 <= ww_R4;
R5 <= ww_R5;
Q1 <= ww_Q1;
Q2 <= ww_Q2;
Q3 <= ww_Q3;
Q4 <= ww_Q4;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_A2~input_o\ <= NOT \A2~input_o\;
\ALT_INV_A3~input_o\ <= NOT \A3~input_o\;
\ALT_INV_B2~input_o\ <= NOT \B2~input_o\;
\ALT_INV_B3~input_o\ <= NOT \B3~input_o\;
\ALT_INV_B1~input_o\ <= NOT \B1~input_o\;
\ALT_INV_B4~input_o\ <= NOT \B4~input_o\;
\ALT_INV_A4~input_o\ <= NOT \A4~input_o\;
\ALT_INV_A1~input_o\ <= NOT \A1~input_o\;
\inst25|inst23|ALT_INV_inst5~combout\ <= NOT \inst25|inst23|inst5~combout\;
\inst39|inst23|ALT_INV_inst3~0_combout\ <= NOT \inst39|inst23|inst3~0_combout\;
\inst37|inst23|ALT_INV_inst5~0_combout\ <= NOT \inst37|inst23|inst5~0_combout\;
\inst35|inst23|ALT_INV_inst5~0_combout\ <= NOT \inst35|inst23|inst5~0_combout\;
\inst35|inst23|ALT_INV_inst8~0_combout\ <= NOT \inst35|inst23|inst8~0_combout\;
\inst32|ALT_INV_inst4~1_combout\ <= NOT \inst32|inst4~1_combout\;
\inst40|inst23|ALT_INV_inst3~2_combout\ <= NOT \inst40|inst23|inst3~2_combout\;
\inst32|ALT_INV_inst4~0_combout\ <= NOT \inst32|inst4~0_combout\;
\inst36|inst23|ALT_INV_inst5~0_combout\ <= NOT \inst36|inst23|inst5~0_combout\;
\inst30|ALT_INV_inst4~0_combout\ <= NOT \inst30|inst4~0_combout\;
\inst31|ALT_INV_inst4~0_combout\ <= NOT \inst31|inst4~0_combout\;
\inst40|inst23|ALT_INV_inst3~1_combout\ <= NOT \inst40|inst23|inst3~1_combout\;
\inst40|inst23|ALT_INV_inst3~0_combout\ <= NOT \inst40|inst23|inst3~0_combout\;
\inst26|ALT_INV_inst4~0_combout\ <= NOT \inst26|inst4~0_combout\;

-- Location: IOOBUF_X0_Y21_N39
\R1~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst34|inst4~0_combout\,
	devoe => ww_devoe,
	o => ww_R1);

-- Location: IOOBUF_X0_Y18_N62
\R2~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst35|inst4~0_combout\,
	devoe => ww_devoe,
	o => ww_R2);

-- Location: IOOBUF_X0_Y21_N56
\R3~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst36|inst4~0_combout\,
	devoe => ww_devoe,
	o => ww_R3);

-- Location: IOOBUF_X0_Y19_N39
\R4~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst37|inst4~0_combout\,
	devoe => ww_devoe,
	o => ww_R4);

-- Location: IOOBUF_X8_Y45_N59
\R5~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => ww_R5);

-- Location: IOOBUF_X10_Y0_N93
\Q1~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst37|inst23|ALT_INV_inst5~0_combout\,
	devoe => ww_devoe,
	o => ww_Q1);

-- Location: IOOBUF_X0_Y18_N96
\Q2~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst40|inst23|ALT_INV_inst3~2_combout\,
	devoe => ww_devoe,
	o => ww_Q2);

-- Location: IOOBUF_X0_Y21_N5
\Q3~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst39|inst23|ALT_INV_inst3~0_combout\,
	devoe => ww_devoe,
	o => ww_Q3);

-- Location: IOOBUF_X0_Y19_N5
\Q4~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst25|inst23|ALT_INV_inst5~combout\,
	devoe => ww_devoe,
	o => ww_Q4);

-- Location: IOIBUF_X0_Y20_N38
\A4~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A4,
	o => \A4~input_o\);

-- Location: IOIBUF_X0_Y20_N55
\B4~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B4,
	o => \B4~input_o\);

-- Location: IOIBUF_X0_Y19_N55
\B1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B1,
	o => \B1~input_o\);

-- Location: IOIBUF_X0_Y20_N4
\B3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B3,
	o => \B3~input_o\);

-- Location: LABCELL_X1_Y20_N3
\inst32|inst4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst32|inst4~0_combout\ = ( !\B4~input_o\ & ( !\B3~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B3~input_o\,
	dataf => \ALT_INV_B4~input_o\,
	combout => \inst32|inst4~0_combout\);

-- Location: IOIBUF_X0_Y20_N21
\B2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B2,
	o => \B2~input_o\);

-- Location: IOIBUF_X0_Y18_N78
\A3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A3,
	o => \A3~input_o\);

-- Location: LABCELL_X1_Y20_N36
\inst40|inst23|inst3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst40|inst23|inst3~0_combout\ = ( \A3~input_o\ & ( \B4~input_o\ & ( (\A4~input_o\ & !\B3~input_o\) ) ) ) # ( !\A3~input_o\ & ( \B4~input_o\ & ( (\A4~input_o\ & !\B3~input_o\) ) ) ) # ( \A3~input_o\ & ( !\B4~input_o\ & ( (\A4~input_o\ & (!\B1~input_o\ & 
-- (!\B3~input_o\ & !\B2~input_o\))) ) ) ) # ( !\A3~input_o\ & ( !\B4~input_o\ & ( (\A4~input_o\ & (!\B3~input_o\ & (!\B1~input_o\ $ (\B2~input_o\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000000010000010000000000000001010000010100000101000001010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A4~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_B3~input_o\,
	datad => \ALT_INV_B2~input_o\,
	datae => \ALT_INV_A3~input_o\,
	dataf => \ALT_INV_B4~input_o\,
	combout => \inst40|inst23|inst3~0_combout\);

-- Location: IOIBUF_X0_Y21_N21
\A2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2,
	o => \A2~input_o\);

-- Location: LABCELL_X1_Y20_N12
\inst40|inst23|inst3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst40|inst23|inst3~1_combout\ = ( \A4~input_o\ & ( !\B4~input_o\ ) ) # ( !\A4~input_o\ & ( !\B4~input_o\ & ( !\B3~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_B3~input_o\,
	datae => \ALT_INV_A4~input_o\,
	dataf => \ALT_INV_B4~input_o\,
	combout => \inst40|inst23|inst3~1_combout\);

-- Location: LABCELL_X1_Y20_N30
\inst26|inst4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst26|inst4~0_combout\ = ( \A3~input_o\ & ( \B4~input_o\ ) ) # ( \A3~input_o\ & ( !\B4~input_o\ & ( (!\B1~input_o\) # (((!\A4~input_o\ & \B2~input_o\)) # (\B3~input_o\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110011111110111100000000000000001111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A4~input_o\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_B3~input_o\,
	datad => \ALT_INV_B2~input_o\,
	datae => \ALT_INV_A3~input_o\,
	dataf => \ALT_INV_B4~input_o\,
	combout => \inst26|inst4~0_combout\);

-- Location: LABCELL_X1_Y20_N6
\inst40|inst23|inst3~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst40|inst23|inst3~2_combout\ = ( \B2~input_o\ & ( \inst26|inst4~0_combout\ & ( (!\inst40|inst23|inst3~1_combout\) # ((!\inst40|inst23|inst3~0_combout\ & (\B1~input_o\ & !\A2~input_o\))) ) ) ) # ( !\B2~input_o\ & ( \inst26|inst4~0_combout\ & ( 
-- !\inst40|inst23|inst3~1_combout\ ) ) ) # ( \B2~input_o\ & ( !\inst26|inst4~0_combout\ & ( (!\inst40|inst23|inst3~0_combout\) # (!\inst40|inst23|inst3~1_combout\) ) ) ) # ( !\B2~input_o\ & ( !\inst26|inst4~0_combout\ & ( (!\inst40|inst23|inst3~1_combout\) 
-- # ((!\inst40|inst23|inst3~0_combout\ & (\B1~input_o\ & !\A2~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100100000111111111010101011111111000000001111111100100000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst40|inst23|ALT_INV_inst3~0_combout\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_A2~input_o\,
	datad => \inst40|inst23|ALT_INV_inst3~1_combout\,
	datae => \ALT_INV_B2~input_o\,
	dataf => \inst26|ALT_INV_inst4~0_combout\,
	combout => \inst40|inst23|inst3~2_combout\);

-- Location: LABCELL_X1_Y20_N42
\inst32|inst4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst32|inst4~1_combout\ = ( \A3~input_o\ & ( \inst40|inst23|inst3~2_combout\ & ( (\A4~input_o\ & ((!\inst32|inst4~0_combout\) # ((!\B1~input_o\ & !\B2~input_o\)))) ) ) ) # ( !\A3~input_o\ & ( \inst40|inst23|inst3~2_combout\ & ( (\A4~input_o\ & 
-- ((!\inst32|inst4~0_combout\) # (!\B1~input_o\ $ (\B2~input_o\)))) ) ) ) # ( \A3~input_o\ & ( !\inst40|inst23|inst3~2_combout\ & ( (\inst32|inst4~0_combout\ & (!\B1~input_o\ & (\A4~input_o\ & !\B2~input_o\))) ) ) ) # ( !\A3~input_o\ & ( 
-- !\inst40|inst23|inst3~2_combout\ & ( (\inst32|inst4~0_combout\ & (!\B1~input_o\ & (\A4~input_o\ & !\B2~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000000000000001000000000000001110000010110000111000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|ALT_INV_inst4~0_combout\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_A4~input_o\,
	datad => \ALT_INV_B2~input_o\,
	datae => \ALT_INV_A3~input_o\,
	dataf => \inst40|inst23|ALT_INV_inst3~2_combout\,
	combout => \inst32|inst4~1_combout\);

-- Location: IOIBUF_X0_Y18_N44
\A1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1,
	o => \A1~input_o\);

-- Location: LABCELL_X1_Y20_N24
\inst30|inst4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst30|inst4~0_combout\ = ( \B2~input_o\ & ( \inst26|inst4~0_combout\ & ( (!\B1~input_o\ & (((\A2~input_o\)))) # (\B1~input_o\ & ((!\A2~input_o\ & (\inst40|inst23|inst3~0_combout\ & \inst40|inst23|inst3~1_combout\)) # (\A2~input_o\ & 
-- ((!\inst40|inst23|inst3~1_combout\))))) ) ) ) # ( !\B2~input_o\ & ( \inst26|inst4~0_combout\ & ( !\A2~input_o\ $ (((!\B1~input_o\) # (!\inst40|inst23|inst3~1_combout\))) ) ) ) # ( \B2~input_o\ & ( !\inst26|inst4~0_combout\ & ( !\A2~input_o\ $ 
-- (((!\inst40|inst23|inst3~0_combout\) # ((!\B1~input_o\) # (!\inst40|inst23|inst3~1_combout\)))) ) ) ) # ( !\B2~input_o\ & ( !\inst26|inst4~0_combout\ & ( (!\B1~input_o\ & (((\A2~input_o\)))) # (\B1~input_o\ & ((!\A2~input_o\ & 
-- (\inst40|inst23|inst3~0_combout\ & \inst40|inst23|inst3~1_combout\)) # (\A2~input_o\ & ((!\inst40|inst23|inst3~1_combout\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100011100000011110001111000001111001111000000111100011100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst40|inst23|ALT_INV_inst3~0_combout\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_A2~input_o\,
	datad => \inst40|inst23|ALT_INV_inst3~1_combout\,
	datae => \ALT_INV_B2~input_o\,
	dataf => \inst26|ALT_INV_inst4~0_combout\,
	combout => \inst30|inst4~0_combout\);

-- Location: LABCELL_X1_Y20_N18
\inst31|inst4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst31|inst4~0_combout\ = ( \B2~input_o\ & ( \inst26|inst4~0_combout\ & ( (!\inst40|inst23|inst3~1_combout\) # ((\B1~input_o\ & !\A2~input_o\)) ) ) ) # ( !\B2~input_o\ & ( \inst26|inst4~0_combout\ & ( (!\B1~input_o\) # ((!\inst40|inst23|inst3~1_combout\) 
-- # (\A2~input_o\)) ) ) ) # ( \B2~input_o\ & ( !\inst26|inst4~0_combout\ & ( (\inst40|inst23|inst3~0_combout\ & (\inst40|inst23|inst3~1_combout\ & ((!\B1~input_o\) # (\A2~input_o\)))) ) ) ) # ( !\B2~input_o\ & ( !\inst26|inst4~0_combout\ & ( 
-- (\inst40|inst23|inst3~0_combout\ & (\B1~input_o\ & (!\A2~input_o\ & \inst40|inst23|inst3~1_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010000000000000100010111111111110011111111111100110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst40|inst23|ALT_INV_inst3~0_combout\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_A2~input_o\,
	datad => \inst40|inst23|ALT_INV_inst3~1_combout\,
	datae => \ALT_INV_B2~input_o\,
	dataf => \inst26|ALT_INV_inst4~0_combout\,
	combout => \inst31|inst4~0_combout\);

-- Location: LABCELL_X2_Y20_N30
\inst36|inst23|inst5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst36|inst23|inst5~0_combout\ = ( \A1~input_o\ & ( \inst31|inst4~0_combout\ & ( (\B3~input_o\ & (\B2~input_o\ & !\inst30|inst4~0_combout\)) ) ) ) # ( !\A1~input_o\ & ( \inst31|inst4~0_combout\ & ( (\B3~input_o\ & ((!\B2~input_o\ & (\B1~input_o\ & 
-- !\inst30|inst4~0_combout\)) # (\B2~input_o\ & ((!\inst30|inst4~0_combout\) # (\B1~input_o\))))) ) ) ) # ( \A1~input_o\ & ( !\inst31|inst4~0_combout\ & ( ((\B2~input_o\ & !\inst30|inst4~0_combout\)) # (\B3~input_o\) ) ) ) # ( !\A1~input_o\ & ( 
-- !\inst31|inst4~0_combout\ & ( ((!\B2~input_o\ & (\B1~input_o\ & !\inst30|inst4~0_combout\)) # (\B2~input_o\ & ((!\inst30|inst4~0_combout\) # (\B1~input_o\)))) # (\B3~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111111101010111011101110101010100010101000000010001000100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B3~input_o\,
	datab => \ALT_INV_B2~input_o\,
	datac => \ALT_INV_B1~input_o\,
	datad => \inst30|ALT_INV_inst4~0_combout\,
	datae => \ALT_INV_A1~input_o\,
	dataf => \inst31|ALT_INV_inst4~0_combout\,
	combout => \inst36|inst23|inst5~0_combout\);

-- Location: LABCELL_X2_Y20_N6
\inst34|inst4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst34|inst4~0_combout\ = ( \A1~input_o\ & ( \inst36|inst23|inst5~0_combout\ & ( ((!\B1~input_o\) # (!\inst32|inst4~1_combout\)) # (\B4~input_o\) ) ) ) # ( !\A1~input_o\ & ( \inst36|inst23|inst5~0_combout\ & ( (!\B4~input_o\ & (\B1~input_o\ & 
-- \inst32|inst4~1_combout\)) ) ) ) # ( \A1~input_o\ & ( !\inst36|inst23|inst5~0_combout\ & ( (!\B1~input_o\) # ((!\A4~input_o\ & \B4~input_o\)) ) ) ) # ( !\A1~input_o\ & ( !\inst36|inst23|inst5~0_combout\ & ( (\B1~input_o\ & ((!\B4~input_o\) # 
-- (\A4~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110100001101111100101111001000000000000011001111111111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A4~input_o\,
	datab => \ALT_INV_B4~input_o\,
	datac => \ALT_INV_B1~input_o\,
	datad => \inst32|ALT_INV_inst4~1_combout\,
	datae => \ALT_INV_A1~input_o\,
	dataf => \inst36|inst23|ALT_INV_inst5~0_combout\,
	combout => \inst34|inst4~0_combout\);

-- Location: LABCELL_X2_Y20_N15
\inst35|inst23|inst8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst35|inst23|inst8~0_combout\ = ( \A1~input_o\ & ( \B2~input_o\ ) ) # ( !\A1~input_o\ & ( !\B1~input_o\ $ (!\B2~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101101001011010000011110000111101011010010110100000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_B2~input_o\,
	datae => \ALT_INV_A1~input_o\,
	combout => \inst35|inst23|inst8~0_combout\);

-- Location: LABCELL_X2_Y20_N51
\inst35|inst4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst35|inst4~0_combout\ = ( \inst32|inst4~1_combout\ & ( \inst35|inst23|inst8~0_combout\ & ( !\inst30|inst4~0_combout\ $ (((\B4~input_o\ & ((!\A4~input_o\) # (\inst36|inst23|inst5~0_combout\))))) ) ) ) # ( !\inst32|inst4~1_combout\ & ( 
-- \inst35|inst23|inst8~0_combout\ & ( !\inst30|inst4~0_combout\ $ ((((!\A4~input_o\ & \B4~input_o\)) # (\inst36|inst23|inst5~0_combout\))) ) ) ) # ( \inst32|inst4~1_combout\ & ( !\inst35|inst23|inst8~0_combout\ & ( \inst30|inst4~0_combout\ ) ) ) # ( 
-- !\inst32|inst4~1_combout\ & ( !\inst35|inst23|inst8~0_combout\ & ( \inst30|inst4~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111000011010010111111000001001011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A4~input_o\,
	datab => \inst36|inst23|ALT_INV_inst5~0_combout\,
	datac => \inst30|ALT_INV_inst4~0_combout\,
	datad => \ALT_INV_B4~input_o\,
	datae => \inst32|ALT_INV_inst4~1_combout\,
	dataf => \inst35|inst23|ALT_INV_inst8~0_combout\,
	combout => \inst35|inst4~0_combout\);

-- Location: LABCELL_X2_Y20_N24
\inst35|inst23|inst5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst35|inst23|inst5~0_combout\ = ( !\A1~input_o\ & ( \inst30|inst4~0_combout\ & ( (\B2~input_o\ & \B1~input_o\) ) ) ) # ( \A1~input_o\ & ( !\inst30|inst4~0_combout\ & ( \B2~input_o\ ) ) ) # ( !\A1~input_o\ & ( !\inst30|inst4~0_combout\ & ( (\B1~input_o\) 
-- # (\B2~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011111100111111001100110011001100000011000000110000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B2~input_o\,
	datac => \ALT_INV_B1~input_o\,
	datae => \ALT_INV_A1~input_o\,
	dataf => \inst30|ALT_INV_inst4~0_combout\,
	combout => \inst35|inst23|inst5~0_combout\);

-- Location: LABCELL_X2_Y20_N0
\inst36|inst4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst36|inst4~0_combout\ = ( \A4~input_o\ & ( \inst31|inst4~0_combout\ & ( !\inst35|inst23|inst5~0_combout\ $ (\B3~input_o\) ) ) ) # ( !\A4~input_o\ & ( \inst31|inst4~0_combout\ & ( (!\inst35|inst23|inst5~0_combout\ $ (\B3~input_o\)) # (\B4~input_o\) ) ) 
-- ) # ( \A4~input_o\ & ( !\inst31|inst4~0_combout\ & ( (!\B4~input_o\ & (\inst32|inst4~1_combout\ & (!\inst35|inst23|inst5~0_combout\ $ (!\B3~input_o\)))) ) ) ) # ( !\A4~input_o\ & ( !\inst31|inst4~0_combout\ & ( (!\B4~input_o\ & (\inst32|inst4~1_combout\ & 
-- (!\inst35|inst23|inst5~0_combout\ $ (!\B3~input_o\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001001000000000000100100010110111101101111010010110100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst35|inst23|ALT_INV_inst5~0_combout\,
	datab => \ALT_INV_B4~input_o\,
	datac => \ALT_INV_B3~input_o\,
	datad => \inst32|ALT_INV_inst4~1_combout\,
	datae => \ALT_INV_A4~input_o\,
	dataf => \inst31|ALT_INV_inst4~0_combout\,
	combout => \inst36|inst4~0_combout\);

-- Location: LABCELL_X2_Y20_N39
\inst37|inst4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst37|inst4~0_combout\ = ( \A4~input_o\ & ( \inst36|inst23|inst5~0_combout\ & ( (\inst32|inst4~1_combout\ & \B4~input_o\) ) ) ) # ( !\A4~input_o\ & ( \inst36|inst23|inst5~0_combout\ & ( (\inst32|inst4~1_combout\ & \B4~input_o\) ) ) ) # ( \A4~input_o\ & 
-- ( !\inst36|inst23|inst5~0_combout\ & ( !\inst32|inst4~1_combout\ $ (!\B4~input_o\) ) ) ) # ( !\A4~input_o\ & ( !\inst36|inst23|inst5~0_combout\ & ( \inst32|inst4~1_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001111000011110000000011000000110000001100000011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \inst32|ALT_INV_inst4~1_combout\,
	datac => \ALT_INV_B4~input_o\,
	datae => \ALT_INV_A4~input_o\,
	dataf => \inst36|inst23|ALT_INV_inst5~0_combout\,
	combout => \inst37|inst4~0_combout\);

-- Location: LABCELL_X2_Y20_N42
\inst37|inst23|inst5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst37|inst23|inst5~0_combout\ = ( \A4~input_o\ & ( \inst36|inst23|inst5~0_combout\ & ( (!\inst32|inst4~1_combout\) # (\B4~input_o\) ) ) ) # ( !\A4~input_o\ & ( \inst36|inst23|inst5~0_combout\ & ( (!\inst32|inst4~1_combout\) # (\B4~input_o\) ) ) ) # ( 
-- !\A4~input_o\ & ( !\inst36|inst23|inst5~0_combout\ & ( \B4~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011000000000000000011111111001100111111111100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_B4~input_o\,
	datad => \inst32|ALT_INV_inst4~1_combout\,
	datae => \ALT_INV_A4~input_o\,
	dataf => \inst36|inst23|ALT_INV_inst5~0_combout\,
	combout => \inst37|inst23|inst5~0_combout\);

-- Location: LABCELL_X1_Y20_N51
\inst39|inst23|inst3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst39|inst23|inst3~0_combout\ = ( \A3~input_o\ & ( (!\inst32|inst4~0_combout\) # ((!\A4~input_o\ & \B2~input_o\)) ) ) # ( !\A3~input_o\ & ( ((!\inst32|inst4~0_combout\) # ((!\A4~input_o\ & \B2~input_o\))) # (\B1~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100101111111111110010001011111111001011111111111100100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A4~input_o\,
	datab => \ALT_INV_B2~input_o\,
	datac => \ALT_INV_B1~input_o\,
	datad => \inst32|ALT_INV_inst4~0_combout\,
	datae => \ALT_INV_A3~input_o\,
	combout => \inst39|inst23|inst3~0_combout\);

-- Location: LABCELL_X1_Y20_N54
\inst25|inst23|inst5\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst25|inst23|inst5~combout\ = ( \B2~input_o\ ) # ( !\B2~input_o\ & ( (!\inst32|inst4~0_combout\) # ((\B1~input_o\ & !\A4~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011101010111010111111111111111110111010101110101111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \inst32|ALT_INV_inst4~0_combout\,
	datab => \ALT_INV_B1~input_o\,
	datac => \ALT_INV_A4~input_o\,
	datae => \ALT_INV_B2~input_o\,
	combout => \inst25|inst23|inst5~combout\);

-- Location: MLABCELL_X18_Y5_N3
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


