-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 20.1.1 Build 720 11/11/2020 SJ Lite Edition"

-- DATE "06/14/2021 12:36:41"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	\aula-07\ IS
    PORT (
	Cout : OUT std_logic;
	A4 : IN std_logic;
	B4 : IN std_logic;
	A3 : IN std_logic;
	B3 : IN std_logic;
	A2 : IN std_logic;
	B2 : IN std_logic;
	A1 : IN std_logic;
	B1 : IN std_logic;
	S4 : OUT std_logic;
	S3 : OUT std_logic;
	S2 : OUT std_logic;
	S1 : OUT std_logic
	);
END \aula-07\;

-- Design Ports Information
-- Cout	=>  Location: PIN_G2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S4	=>  Location: PIN_N1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S3	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S2	=>  Location: PIN_G1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S1	=>  Location: PIN_Y3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A1	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B1	=>  Location: PIN_U1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A2	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B2	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A3	=>  Location: PIN_D3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B3	=>  Location: PIN_AA2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A4	=>  Location: PIN_U2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B4	=>  Location: PIN_N2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF \aula-07\ IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_Cout : std_logic;
SIGNAL ww_A4 : std_logic;
SIGNAL ww_B4 : std_logic;
SIGNAL ww_A3 : std_logic;
SIGNAL ww_B3 : std_logic;
SIGNAL ww_A2 : std_logic;
SIGNAL ww_B2 : std_logic;
SIGNAL ww_A1 : std_logic;
SIGNAL ww_B1 : std_logic;
SIGNAL ww_S4 : std_logic;
SIGNAL ww_S3 : std_logic;
SIGNAL ww_S2 : std_logic;
SIGNAL ww_S1 : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \B2~input_o\ : std_logic;
SIGNAL \B4~input_o\ : std_logic;
SIGNAL \A3~input_o\ : std_logic;
SIGNAL \A4~input_o\ : std_logic;
SIGNAL \B3~input_o\ : std_logic;
SIGNAL \inst2|inst18~0_combout\ : std_logic;
SIGNAL \A1~input_o\ : std_logic;
SIGNAL \B1~input_o\ : std_logic;
SIGNAL \A2~input_o\ : std_logic;
SIGNAL \inst4|inst18~0_combout\ : std_logic;
SIGNAL \inst6|inst3~0_combout\ : std_logic;
SIGNAL \inst2|inst14~0_combout\ : std_logic;
SIGNAL \inst3|inst14~0_combout\ : std_logic;
SIGNAL \inst4|inst14~0_combout\ : std_logic;
SIGNAL \ALT_INV_B4~input_o\ : std_logic;
SIGNAL \ALT_INV_A4~input_o\ : std_logic;
SIGNAL \ALT_INV_B3~input_o\ : std_logic;
SIGNAL \ALT_INV_A3~input_o\ : std_logic;
SIGNAL \ALT_INV_B2~input_o\ : std_logic;
SIGNAL \ALT_INV_A2~input_o\ : std_logic;
SIGNAL \ALT_INV_B1~input_o\ : std_logic;
SIGNAL \ALT_INV_A1~input_o\ : std_logic;
SIGNAL \inst2|ALT_INV_inst18~0_combout\ : std_logic;

BEGIN

Cout <= ww_Cout;
ww_A4 <= A4;
ww_B4 <= B4;
ww_A3 <= A3;
ww_B3 <= B3;
ww_A2 <= A2;
ww_B2 <= B2;
ww_A1 <= A1;
ww_B1 <= B1;
S4 <= ww_S4;
S3 <= ww_S3;
S2 <= ww_S2;
S1 <= ww_S1;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_B4~input_o\ <= NOT \B4~input_o\;
\ALT_INV_A4~input_o\ <= NOT \A4~input_o\;
\ALT_INV_B3~input_o\ <= NOT \B3~input_o\;
\ALT_INV_A3~input_o\ <= NOT \A3~input_o\;
\ALT_INV_B2~input_o\ <= NOT \B2~input_o\;
\ALT_INV_A2~input_o\ <= NOT \A2~input_o\;
\ALT_INV_B1~input_o\ <= NOT \B1~input_o\;
\ALT_INV_A1~input_o\ <= NOT \A1~input_o\;
\inst2|ALT_INV_inst18~0_combout\ <= NOT \inst2|inst18~0_combout\;

-- Location: IOOBUF_X0_Y21_N5
\Cout~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|inst18~0_combout\,
	devoe => ww_devoe,
	o => ww_Cout);

-- Location: IOOBUF_X0_Y19_N56
\S4~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst6|inst3~0_combout\,
	devoe => ww_devoe,
	o => ww_S4);

-- Location: IOOBUF_X0_Y20_N39
\S3~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst2|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_S3);

-- Location: IOOBUF_X0_Y21_N22
\S2~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst3|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_S2);

-- Location: IOOBUF_X0_Y18_N45
\S1~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst4|inst14~0_combout\,
	devoe => ww_devoe,
	o => ww_S1);

-- Location: IOIBUF_X0_Y20_N55
\B2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B2,
	o => \B2~input_o\);

-- Location: IOIBUF_X0_Y19_N38
\B4~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B4,
	o => \B4~input_o\);

-- Location: IOIBUF_X0_Y20_N4
\A3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A3,
	o => \A3~input_o\);

-- Location: IOIBUF_X0_Y19_N4
\A4~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A4,
	o => \A4~input_o\);

-- Location: IOIBUF_X0_Y18_N78
\B3~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B3,
	o => \B3~input_o\);

-- Location: LABCELL_X1_Y20_N30
\inst2|inst18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|inst18~0_combout\ = ( \B3~input_o\ & ( ((\B4~input_o\ & \A4~input_o\)) # (\A3~input_o\) ) ) # ( !\B3~input_o\ & ( (\B4~input_o\ & (\A3~input_o\ & \A4~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100000001001101110011011100000001000000010011011100110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B4~input_o\,
	datab => \ALT_INV_A3~input_o\,
	datac => \ALT_INV_A4~input_o\,
	datae => \ALT_INV_B3~input_o\,
	combout => \inst2|inst18~0_combout\);

-- Location: IOIBUF_X0_Y21_N55
\A1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A1,
	o => \A1~input_o\);

-- Location: IOIBUF_X0_Y19_N21
\B1~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B1,
	o => \B1~input_o\);

-- Location: IOIBUF_X0_Y20_N21
\A2~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A2,
	o => \A2~input_o\);

-- Location: LABCELL_X1_Y20_N39
\inst4|inst18~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|inst18~0_combout\ = ( \A2~input_o\ & ( (!\A1~input_o\ & (\B1~input_o\ & ((\inst2|inst18~0_combout\) # (\B2~input_o\)))) # (\A1~input_o\ & (((\B1~input_o\) # (\inst2|inst18~0_combout\)) # (\B2~input_o\))) ) ) # ( !\A2~input_o\ & ( (!\A1~input_o\ & 
-- (\B2~input_o\ & (\inst2|inst18~0_combout\ & \B1~input_o\))) # (\A1~input_o\ & (((\B2~input_o\ & \inst2|inst18~0_combout\)) # (\B1~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100011111000000010001111100000111011111110000011101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \inst2|ALT_INV_inst18~0_combout\,
	datac => \ALT_INV_A1~input_o\,
	datad => \ALT_INV_B1~input_o\,
	dataf => \ALT_INV_A2~input_o\,
	combout => \inst4|inst18~0_combout\);

-- Location: LABCELL_X1_Y19_N0
\inst6|inst3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst6|inst3~0_combout\ = ( \B4~input_o\ & ( !\A4~input_o\ ) ) # ( !\B4~input_o\ & ( \A4~input_o\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111111110000111100001111000011110000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_A4~input_o\,
	dataf => \ALT_INV_B4~input_o\,
	combout => \inst6|inst3~0_combout\);

-- Location: LABCELL_X1_Y20_N15
\inst2|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst2|inst14~0_combout\ = ( \B3~input_o\ & ( !\A3~input_o\ $ (((\A4~input_o\ & \B4~input_o\))) ) ) # ( !\B3~input_o\ & ( !\A3~input_o\ $ (((!\A4~input_o\) # (!\B4~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111101011010111100001010010100001111010110101111000010100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A4~input_o\,
	datac => \ALT_INV_A3~input_o\,
	datad => \ALT_INV_B4~input_o\,
	datae => \ALT_INV_B3~input_o\,
	combout => \inst2|inst14~0_combout\);

-- Location: LABCELL_X1_Y20_N18
\inst3|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst3|inst14~0_combout\ = ( \B2~input_o\ & ( \A2~input_o\ & ( (!\A3~input_o\ & (\B4~input_o\ & (\A4~input_o\ & \B3~input_o\))) # (\A3~input_o\ & (((\B4~input_o\ & \A4~input_o\)) # (\B3~input_o\))) ) ) ) # ( !\B2~input_o\ & ( \A2~input_o\ & ( 
-- (!\A3~input_o\ & ((!\B4~input_o\) # ((!\A4~input_o\) # (!\B3~input_o\)))) # (\A3~input_o\ & (!\B3~input_o\ & ((!\B4~input_o\) # (!\A4~input_o\)))) ) ) ) # ( \B2~input_o\ & ( !\A2~input_o\ & ( (!\A3~input_o\ & ((!\B4~input_o\) # ((!\A4~input_o\) # 
-- (!\B3~input_o\)))) # (\A3~input_o\ & (!\B3~input_o\ & ((!\B4~input_o\) # (!\A4~input_o\)))) ) ) ) # ( !\B2~input_o\ & ( !\A2~input_o\ & ( (!\A3~input_o\ & (\B4~input_o\ & (\A4~input_o\ & \B3~input_o\))) # (\A3~input_o\ & (((\B4~input_o\ & \A4~input_o\)) # 
-- (\B3~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100110111111111101100100011111110110010000000000100110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B4~input_o\,
	datab => \ALT_INV_A3~input_o\,
	datac => \ALT_INV_A4~input_o\,
	datad => \ALT_INV_B3~input_o\,
	datae => \ALT_INV_B2~input_o\,
	dataf => \ALT_INV_A2~input_o\,
	combout => \inst3|inst14~0_combout\);

-- Location: LABCELL_X1_Y20_N36
\inst4|inst14~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst4|inst14~0_combout\ = ( \A2~input_o\ & ( !\B1~input_o\ $ (!\A1~input_o\ $ (((\inst2|inst18~0_combout\) # (\B2~input_o\)))) ) ) # ( !\A2~input_o\ & ( !\B1~input_o\ $ (!\A1~input_o\ $ (((\B2~input_o\ & \inst2|inst18~0_combout\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001111011100001000111101110000101111000100001110111100010000111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B2~input_o\,
	datab => \inst2|ALT_INV_inst18~0_combout\,
	datac => \ALT_INV_B1~input_o\,
	datad => \ALT_INV_A1~input_o\,
	dataf => \ALT_INV_A2~input_o\,
	combout => \inst4|inst14~0_combout\);

-- Location: LABCELL_X21_Y38_N3
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


