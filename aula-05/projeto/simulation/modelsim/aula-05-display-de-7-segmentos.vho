-- Copyright (C) 2020  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 20.1.1 Build 720 11/11/2020 SJ Lite Edition"

-- DATE "05/26/2021 17:12:43"

-- 
-- Device: Altera 5CEBA4F23C7 Package FBGA484
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	\aula-05-display-de-7-segmentos\ IS
    PORT (
	\seg-1\ : OUT std_logic;
	A : IN std_logic;
	C : IN std_logic;
	D : IN std_logic;
	B : IN std_logic;
	\seg-0\ : OUT std_logic;
	\seg-2\ : OUT std_logic;
	\seg-3\ : OUT std_logic;
	\seg-4\ : OUT std_logic;
	\seg-5\ : OUT std_logic;
	\seg-6\ : OUT std_logic
	);
END \aula-05-display-de-7-segmentos\;

-- Design Ports Information
-- seg-1	=>  Location: PIN_AA1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg-0	=>  Location: PIN_D3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg-2	=>  Location: PIN_AA2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg-3	=>  Location: PIN_C1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg-4	=>  Location: PIN_G1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg-5	=>  Location: PIN_L1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- seg-6	=>  Location: PIN_E2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- A	=>  Location: PIN_U2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- C	=>  Location: PIN_U1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D	=>  Location: PIN_L2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- B	=>  Location: PIN_N2,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF \aula-05-display-de-7-segmentos\ IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL \ww_seg-1\ : std_logic;
SIGNAL ww_A : std_logic;
SIGNAL ww_C : std_logic;
SIGNAL ww_D : std_logic;
SIGNAL ww_B : std_logic;
SIGNAL \ww_seg-0\ : std_logic;
SIGNAL \ww_seg-2\ : std_logic;
SIGNAL \ww_seg-3\ : std_logic;
SIGNAL \ww_seg-4\ : std_logic;
SIGNAL \ww_seg-5\ : std_logic;
SIGNAL \ww_seg-6\ : std_logic;
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \B~input_o\ : std_logic;
SIGNAL \C~input_o\ : std_logic;
SIGNAL \A~input_o\ : std_logic;
SIGNAL \D~input_o\ : std_logic;
SIGNAL \inst23~0_combout\ : std_logic;
SIGNAL \inst15~0_combout\ : std_logic;
SIGNAL \inst34~0_combout\ : std_logic;
SIGNAL \inst50~0_combout\ : std_logic;
SIGNAL \inst56~0_combout\ : std_logic;
SIGNAL \inst66~0_combout\ : std_logic;
SIGNAL \inst77~0_combout\ : std_logic;
SIGNAL \ALT_INV_B~input_o\ : std_logic;
SIGNAL \ALT_INV_D~input_o\ : std_logic;
SIGNAL \ALT_INV_C~input_o\ : std_logic;
SIGNAL \ALT_INV_A~input_o\ : std_logic;
SIGNAL \ALT_INV_inst50~0_combout\ : std_logic;
SIGNAL \ALT_INV_inst15~0_combout\ : std_logic;

BEGIN

\seg-1\ <= \ww_seg-1\;
ww_A <= A;
ww_C <= C;
ww_D <= D;
ww_B <= B;
\seg-0\ <= \ww_seg-0\;
\seg-2\ <= \ww_seg-2\;
\seg-3\ <= \ww_seg-3\;
\seg-4\ <= \ww_seg-4\;
\seg-5\ <= \ww_seg-5\;
\seg-6\ <= \ww_seg-6\;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_B~input_o\ <= NOT \B~input_o\;
\ALT_INV_D~input_o\ <= NOT \D~input_o\;
\ALT_INV_C~input_o\ <= NOT \C~input_o\;
\ALT_INV_A~input_o\ <= NOT \A~input_o\;
\ALT_INV_inst50~0_combout\ <= NOT \inst50~0_combout\;
\ALT_INV_inst15~0_combout\ <= NOT \inst15~0_combout\;

-- Location: IOOBUF_X0_Y18_N96
\seg-1~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst23~0_combout\,
	devoe => ww_devoe,
	o => \ww_seg-1\);

-- Location: IOOBUF_X0_Y20_N5
\seg-0~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_inst15~0_combout\,
	devoe => ww_devoe,
	o => \ww_seg-0\);

-- Location: IOOBUF_X0_Y18_N79
\seg-2~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst34~0_combout\,
	devoe => ww_devoe,
	o => \ww_seg-2\);

-- Location: IOOBUF_X0_Y21_N56
\seg-3~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \ALT_INV_inst50~0_combout\,
	devoe => ww_devoe,
	o => \ww_seg-3\);

-- Location: IOOBUF_X0_Y21_N22
\seg-4~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst56~0_combout\,
	devoe => ww_devoe,
	o => \ww_seg-4\);

-- Location: IOOBUF_X0_Y20_N56
\seg-5~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst66~0_combout\,
	devoe => ww_devoe,
	o => \ww_seg-5\);

-- Location: IOOBUF_X0_Y20_N22
\seg-6~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \inst77~0_combout\,
	devoe => ww_devoe,
	o => \ww_seg-6\);

-- Location: IOIBUF_X0_Y19_N38
\B~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_B,
	o => \B~input_o\);

-- Location: IOIBUF_X0_Y19_N21
\C~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_C,
	o => \C~input_o\);

-- Location: IOIBUF_X0_Y19_N4
\A~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_A,
	o => \A~input_o\);

-- Location: IOIBUF_X0_Y20_N38
\D~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D,
	o => \D~input_o\);

-- Location: LABCELL_X1_Y20_N0
\inst23~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst23~0_combout\ = ( \D~input_o\ & ( (!\C~input_o\ & ((!\B~input_o\) # (\A~input_o\))) # (\C~input_o\ & ((!\A~input_o\))) ) ) # ( !\D~input_o\ & ( (!\B~input_o\) # ((!\C~input_o\ & !\A~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1110101011101010111010101110101010111100101111001011110010111100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B~input_o\,
	datab => \ALT_INV_C~input_o\,
	datac => \ALT_INV_A~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \inst23~0_combout\);

-- Location: LABCELL_X1_Y20_N9
\inst15~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst15~0_combout\ = ( \D~input_o\ & ( (!\A~input_o\ & (!\C~input_o\ & !\B~input_o\)) # (\A~input_o\ & (!\C~input_o\ $ (!\B~input_o\))) ) ) # ( !\D~input_o\ & ( (!\A~input_o\ & (!\C~input_o\ & \B~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010100000000000001010000010100101010100001010010101010000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A~input_o\,
	datac => \ALT_INV_C~input_o\,
	datad => \ALT_INV_B~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \inst15~0_combout\);

-- Location: LABCELL_X1_Y20_N12
\inst34~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst34~0_combout\ = ( \D~input_o\ & ( (!\B~input_o\) # ((!\C~input_o\) # (!\A~input_o\)) ) ) # ( !\D~input_o\ & ( (!\B~input_o\ & ((!\C~input_o\) # (\A~input_o\))) # (\B~input_o\ & ((!\A~input_o\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1101101011011010110110101101101011111110111111101111111011111110",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B~input_o\,
	datab => \ALT_INV_C~input_o\,
	datac => \ALT_INV_A~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \inst34~0_combout\);

-- Location: LABCELL_X1_Y20_N21
\inst50~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst50~0_combout\ = ( \D~input_o\ & ( (!\C~input_o\ & (!\A~input_o\ & !\B~input_o\)) # (\C~input_o\ & ((\B~input_o\))) ) ) # ( !\D~input_o\ & ( (!\A~input_o\ & (!\C~input_o\ & \B~input_o\)) # (\A~input_o\ & (\C~input_o\ & !\B~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010110100000000001011010000010100000000011111010000000001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A~input_o\,
	datac => \ALT_INV_C~input_o\,
	datad => \ALT_INV_B~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \inst50~0_combout\);

-- Location: LABCELL_X1_Y20_N24
\inst56~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst56~0_combout\ = ( \D~input_o\ & ( (\A~input_o\ & ((\C~input_o\) # (\B~input_o\))) ) ) # ( !\D~input_o\ & ( (!\B~input_o\) # ((\A~input_o\) # (\C~input_o\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1011111110111111101111111011111100000111000001110000011100000111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B~input_o\,
	datab => \ALT_INV_C~input_o\,
	datac => \ALT_INV_A~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \inst56~0_combout\);

-- Location: LABCELL_X1_Y20_N33
\inst66~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst66~0_combout\ = ( \D~input_o\ & ( !\A~input_o\ $ (((!\B~input_o\) # (\C~input_o\))) ) ) # ( !\D~input_o\ & ( ((!\C~input_o\) # (\B~input_o\)) # (\A~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111010111111111111101011111111101010101101001010101010110100101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_A~input_o\,
	datac => \ALT_INV_C~input_o\,
	datad => \ALT_INV_B~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \inst66~0_combout\);

-- Location: LABCELL_X1_Y20_N36
\inst77~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \inst77~0_combout\ = ( \D~input_o\ & ( (!\B~input_o\ $ (!\C~input_o\)) # (\A~input_o\) ) ) # ( !\D~input_o\ & ( (!\B~input_o\ $ (!\A~input_o\)) # (\C~input_o\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0111101101111011011110110111101101101111011011110110111101101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_B~input_o\,
	datab => \ALT_INV_C~input_o\,
	datac => \ALT_INV_A~input_o\,
	dataf => \ALT_INV_D~input_o\,
	combout => \inst77~0_combout\);

-- Location: LABCELL_X25_Y16_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


